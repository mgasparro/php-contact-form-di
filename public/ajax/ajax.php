<?php

include_once '../include.php';

if( true === isset( $_POST ) && false === empty( $_POST ) ) {
	
	$a_inputFields = array(
						'fullName'	=> 'required',
						'email'		=> 'required',
						'message'	=> 'required',
						'phone'		=> 'false'
					);
	
	
	/** ********** VALIDATE FORM ********** **/
	$o_validator = new Validator();
	$b_isFormValid = $o_validator->validateFields( $_POST, $a_inputFields );
	
	if( true === is_array( $b_isFormValid[0] ) || false == $b_isFormValid ) {
		if( false == $b_isFormValid ) {
			die( json_encode( 'false' ) );
		}
		die( json_encode( $b_isFormValid[0] ) );
	}
	
	
	/** ********** EMAIL FORM ********** **/
	$b_emailSent = 'false';
	$o_email = new Emailer( $_POST );
	if( false === empty( $o_email ) ) {
		$a_formattedEmails = $o_email->formatEmail();
		if( false != $a_formattedEmails ) {
			$b_emailSent = $o_email->sendEmail( $a_formattedEmails['message'], $a_formattedEmails['plainTextMessage'] );
		}
	}
	
	$a_userInfo = array();
	$a_userInfo['user_id'] = $_SERVER['REMOTE_ADDR'];
	$a_userInfo['user_agent'] = $_SERVER['HTTP_USER_AGENT'];
	$a_userInfo['user_referer'] = $_SERVER['HTTP_REFERER'];
	
	$a_submissionData = array_merge( $_POST, $a_userInfo );
	$a_submissionData['emailSent'] = $b_emailSent;
	
	
	/** ********** INSERT FORM ********** **/
	$o_db = new DB();
	$db_result = $o_db->insertSubmission( $a_submissionData );

	if( true == $db_result ) {
		die( json_encode( 'true' ) );
	}
	
	die( json_encode( 'false' ) );
	
}else{
	
	die( json_encode( 'false' ) );
	
}

die();

?>