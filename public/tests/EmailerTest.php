<?php

use PHPUnit\Framework\TestCase;

class EmailerTest extends TestCase {
	
	public function testEmailerInstance() {
		
		$a_formData = array(
			'fullName'	=> 'Guy Smiley',
			'email'		=> 'test@test.com',
			'message'	=> 'This message is for Guy Smiley.',
		);
		
		$o_emailer = new Emailer( $a_formData );
		$this->assertInstanceOf(
			'Emailer',
			$o_emailer,
			'Emailer instance does not exist'
		);
		
		return $o_emailer;
	
	}
	
	/**
	 * @depends testEmailerInstance
	 */
	public function testFormatEmail( $o_emailer ) {
		
		$a_formattedEmails = $o_emailer->formatEmail();
		$this->assertNotFalse( 
			$a_formattedEmails,
			'Could not format email'
		);
		
		return $a_formattedEmails;
		
	}
	
	/**
	 * @depends testEmailerInstance
	 * @depends testFormatEmail
	 */
	public function testSendEmailSent( $o_emailer, $a_formattedEmails ) {
		
		$this->assertNotFalse(
			$o_emailer->sendEmail( $a_formattedEmails['message'], $a_formattedEmails['plainTextMessage'] ),
			'Form failed to send'
		);
		
	}
	
	/**
	 * @depends testEmailerInstance
	 */
	public function testSendEmailSentFailed( $o_emailer ) {
	
		$this->assertFalse(
			$o_emailer->sendEmail( '', '' ),
			'Form failed to send'
		);
	
	}
	
}