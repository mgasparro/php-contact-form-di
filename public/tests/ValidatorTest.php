<?php

use PHPUnit\Framework\TestCase;

class ValidatorTest extends TestCase {
	
	public function testValidatorInstance() {
		$o_validator = new Validator();
		$this->assertInstanceOf( 
			'Validator', 
			$o_validator,
			'Validator instance does not exist'
		);
		
		return $o_validator;
	}
	
	/**
	 * @depends testValidatorInstance
	 */
	public function testFullNameValid( $o_validator ) {
		$this->assertEquals( 
			true, 
			$o_validator->validateFullName( 'Guy Smiley' ),
			'Full Name is valid'
		);
	}
	
	/**
	 * @depends testValidatorInstance
	 */
	public function testFullNameInvalid( $o_validator ) {
		$this->assertEquals( 
			false, 
			$o_validator->validateFullName( 'Guy' ),
			'Full Name is invalid'
		);
	}
	
	/**
	 * @depends testValidatorInstance
	 */
	public function testFullNameInvalidSpecialChars( $o_validator ) {
		$this->assertEquals(
			false,
			$o_validator->validateFullName( '** *' ),
			'Full Name is invalid'
		);
	}
	
	/**
	 * @depends testValidatorInstance
	 */
	public function testEmailValid( $o_validator ) {
		$this->assertEquals( 
			true,
			$o_validator->validateEmail( 'test@test.com' ),
			'Email is valid'
		);
	}
	
	/**
	 * @depends testValidatorInstance
	 */
	public function testEmailInvalid( $o_validator ) {
		$this->assertEquals( 
			false, 
			$o_validator->validateEmail( 'test@test' ),
			'Email is invalid'
		);
	}
	
	/**
	 * @depends testValidatorInstance
	 */
	public function testPhoneValid( $o_validator ) {
		$this->assertEquals( 
			true,
			$o_validator->validatePhone( '(123) 123 - 1234' ),
			'Phone number is valid'
		);
	}
	
	/**
	 * @depends testValidatorInstance
	 */
	public function testPhoneInvalid( $o_validator ) {
		$this->assertEquals(
			false,
			$o_validator->validatePhone( '234&234' ),
			'Phone number is invalid'
		);
	}
	
	/**
	 * @depends testValidatorInstance
	 */
	public function testMessageValid( $o_validator ) {
		$this->assertEquals( 
			true,
			$o_validator->validateMessage( 'This message is for Guy Smiley.' ),
			'Message is valid'
		);
	}
	
	/**
	 * @depends testValidatorInstance
	 */
	public function testMessageInvalid( $o_validator ) {
		$this->assertEquals( 
			false, 
			$o_validator->validateMessage( 'a' ),
			'Message is invalid'
		);
	}
	
	/**
	 * @depends testValidatorInstance
	 */
	public function testValidateFieldsValid( $o_validator ) {
		$a_inputtedFormData = array( 
			'fullName'	=> 'Guy Smiley',
			'email'		=> 'test@test.com',
			'message'	=> 'This message is for Guy Smiley.'
		);
		
		$a_inputFields = array(
			'fullName'	=> 'required',
			'email'		=> 'required',
			'message'	=> 'required',
			'phone'		=> 'false'
		);
		
		$this->assertEquals(
			true,
			$o_validator->validateFields( $a_inputtedFormData, $a_inputFields ),
			'Form is valid'
		);
	}
	
	/**
	 * @depends testValidatorInstance
	 */
	public function testValidateFieldsInvalid( $o_validator ) {
		$a_inputFields = array(
			'fullName'	=> 'required',
			'email'		=> 'required',
			'message'	=> 'required',
			'phone'		=> 'false'
		);
	
		$this->assertEquals(
			false,
			$o_validator->validateFields( array(), $a_inputFields ),
			'Form is invalid'
		);
	}
	
	/**
	 * @depends testValidatorInstance
	 */
	public function testValidateFieldsInvalidError( $o_validator ) {
		$a_inputtedFormData = array(
			'fullName'	=> 'Guy Smiley',
			'email'		=> 'test@test',
			'message'	=> 'This message is for Guy Smiley.'
		);
	
		$a_inputFields = array(
			'fullName'	=> 'required',
			'email'		=> 'required',
			'message'	=> 'required',
			'phone'		=> 'false'
		);
	
		$a_expectedResult = array(
			array( 
				'email' => 'This field is not valid'
			)
		);
	
		$this->assertEquals(
			$a_expectedResult,
			$o_validator->validateFields( $a_inputtedFormData, $a_inputFields ),
			'Form is invalid'
		);
	}
	
	/**
	 * @depends testValidatorInstance
	 */
	public function testValidateFieldsInvalidRequiredError( $o_validator ) {
		$a_inputtedFormData = array(
			'fullName'	=> '',
			'email'		=> 'test@test.com',
			'message'	=> 'This message is for Guy Smiley.',
			'phone'		=> '1235551234'
		);
		
		$a_inputFields = array(
			'fullName'	=> 'required',
			'email'		=> 'required',
			'message'	=> 'required',
			'phone'		=> 'false'
		);
		
		$a_expectedResult = array(
			array( 
				'fullName' => 'This field is required'
			)
		);
	
		$this->assertEquals(
			$a_expectedResult,
			$o_validator->validateFields( $a_inputtedFormData, $a_inputFields ),
			'Form is invalid'
		);
	}
	
	/**
	 * @depends testValidatorInstance
	 */
	public function testValidateFieldsInvalidAndRequiredError( $o_validator ) {
		$a_inputtedFormData = array(
			'fullName'	=> 'Guy Smiley',
			'email'		=> 'test@test',
			'message'	=> ''
		);
	
		$a_inputFields = array(
			'fullName'	=> 'required',
			'email'		=> 'required',
			'message'	=> 'required',
			'phone'		=> 'false'
		);
	
		$a_expectedResult = array(
			array( 
				'email'		=> 'This field is not valid',
				'message'	=> 'This field is required'
			)
		);
	
		$this->assertEquals(
			$a_expectedResult,
			$o_validator->validateFields( $a_inputtedFormData, $a_inputFields ),
			'Form is invalid'
		);
	}
	
	/**
	 * @depends testValidatorInstance
	 */
	public function testValidateFieldsInvalidMethodError( $o_validator ) {
		$a_inputtedFormData = array(
			'fullName'	=> 'Guy Smiley',
			'email'		=> 'test@test.com',
			'message'	=> 'This message is for Guy Smiley.',
			'address'	=> '123 Guy Dr.'
		);
	
		$a_inputFields = array(
			'fullName'	=> 'required',
			'email'		=> 'required',
			'message'	=> 'required',
			'phone'		=> 'false'
		);
	
		$a_expectedResult = array(
			array(
				'address' => 'This field is not valid'
			)
		);
	
		$this->assertEquals(
				$a_expectedResult,
				$o_validator->validateFields( $a_inputtedFormData, $a_inputFields ),
				'Form is invalid'
		);
	}
	
	
}