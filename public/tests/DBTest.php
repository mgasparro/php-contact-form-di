<?php

use PHPUnit\Framework\TestCase;

class DBTest extends TestCase {
	
	public function testDBInstance() {
	
		$o_db = new DB();
		$this->assertInstanceOf(
			'DB',
			$o_db,
			'DB instance does not exist'
		);
	
		return $o_db;
	
	}
	
	/**
	 * @depends testDBInstance
	 */
	public function testInsertSubmission( $o_db ) {
	
		$a_submissionData = array(
			'fullName'		=> 'Guy Smiley',
			'email'			=> 'test@test.com',
			'phone'			=> '',
			'message'		=> 'This message is for Guy Smiley.',
			'user_id'		=> '',
			'user_agent'	=> '',
			'user_referer'	=> '',
			'emailSent'		=> 'false'
		);
	
		$this->assertEquals(
				true,
				$o_db->insertSubmission( $a_submissionData ),
				'Error inserting into the database'
		);
	
	}
	
	/**
	 * @depends testDBInstance
	 */
	public function testInsertSubmissionInvalid( $o_db ) {
	
		$this->assertEquals(
				null,
				$o_db->insertSubmission( '' ),
				'Error inserting into the database'
		);
	
	}
	
}