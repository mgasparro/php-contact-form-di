<?php
/**
 *
 * This class provides the constants needed for the Emailer and DB classes
 *
 * @author Marissa Gasparro
 * @version 1.0
 *
 */
abstract class Constants {
	
	//Email Creds
	protected $s_emailHost = 'smtp.gmail.com';				// Specify main and backup SMTP servers
	protected $s_smtpAuth = 'true';							// Enable SMTP authentication
	protected $s_emailUsername = 'user@example.com';		// SMTP username
	protected $s_emailPass = 'apassword';					// SMTP password
	protected $s_smtpSecure = 'ssl';						// Enable TLS encryption, `ssl` also accepted
	protected $i_emailPort = 465;							// Port to connect to (587 for TLS)
	
	protected $s_toEmail = 'guy-smiley@example.com';
	protected $s_fromName = 'Another User';
	protected $s_fromEmail = 'anotherUser@example.com';
	protected $s_subject = 'Contact Guy Smiley';
	
	// DB Creds
	protected $s_dbHost = 'localhost';
	protected $s_dbUsername = 'dichallenge_user';
	protected $s_dbPass = '%NAP6s;MH4B\V3_!';
	protected $s_dbName = 'dichallenge_db';
	protected $s_dbPort = '3306';
	
	protected $o_dbLink;
	protected $s_errorMessage;
	protected $b_isConnected;
	
	protected $s_submissionsColumns =
				'id, full_name, email, phone, message, user_ip, user_agent, user_referer, email_sent, to_email, ts_created';
	
	public function __construct(){}
	
	public function __destruct(){}
	
	/**
	 * Generic accessor method
	 * 
	 * @param string $s_property
	 * @return NULL | property value
	 */
	protected function getProperty( $s_property = null ) {
		if( !is_null( $s_property ) && isset( $this->$s_property ) ) {
			return $this->$s_property;
		} else {
			return null;
		}
	}
	
	/**
	 * Generic mutator method
	 * 
	 * @param string $s_property
	 * @param string/int $s_value
	 */
	protected function setProperty( $s_property, $s_value ) {
		if( 'i_port' == $s_property && true == is_numeric( $s_value ) ) {
			$s_value = (int)$s_value;
		}
		$this->$s_property = $s_value;
	}
	
}