<?php
/**
 *
 * This class connects to a db and can perform various queries
 *
 * @author Marissa Gasparro
 * @version 1.0
 *
 */
class DB extends Constants {
	
	public function __construct(){
		$this->connect();
	}
	
	public function __destruct(){
		@$this->o_dbLink->close();
		unset($this->o_dbLink);
	}
	
	/**
	 * This function connects to a db with the given credentials
	 */
	private function connect() {
		$this->o_dbLink = @new mysqli($this->s_dbHost, $this->s_dbUsername, $this->s_dbPass, $this->s_dbName, $this->s_dbPort);
	
		if($this->o_dbLink->connect_errno) {
			$this->b_isConnected = false;
			$this->s_errorMessage = $this->o_dbLink->connect_error;
		} else {
			$this->b_isConnected = true;
		}
	}
	
	/**
	 * This function inserts a contact form submission to the submissions table
	 * 
	 * @param array $a_subData
	 * @return NULL|boolean
	 */
	public function insertSubmission( $a_subData ){
		
		if( $this->b_isConnected ) {
			
			$s_escSubData = '';
			
			if( true === is_array( $a_subData ) ) {
				
				foreach( $a_subData as $s_subKey => $s_subValue ) {
					$s_tempValue = $this->o_dbLink->real_escape_string( $s_subValue );
					$s_escSubData .= "'$s_tempValue', ";
				}
				
				$s_escSubData .= "'$this->s_toEmail', ";
				
				$s_SQL = "INSERT INTO $this->s_dbName.submissions ( $this->s_submissionsColumns )
							VALUES( NULL, $s_escSubData NULL )";
				
				$sql_result = $this->o_dbLink->query($s_SQL);
				
				if( true != $sql_result ) {
					$this->s_errorMessage = $this->o_dbLink->connect_error;
					return null;
				} else {
					return true;
				}
				
			}
			
			return null;
		
		}else{
			
			return null;
			
		}
		
	}
	
}