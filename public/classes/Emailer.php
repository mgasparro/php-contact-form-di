<?php
/**
 *
 * This class can format and send an THML/plain text email
 *
 * @author Marissa Gasparro
 * @version 1.0
 *
 */

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

class Emailer extends Constants {
	
	private $a_displayNamesAndValues = array();
	
	/**
	 * This function ensures there is form data to email and creates 
	 * names that are better suited to be displayed
	 * 
	 * @param array $a_formData
	 * @return boolean
	 */
	public function __construct( $a_formData ) {
		
		if( true === is_array( $a_formData ) ) {
			$this->a_displayNamesAndValues = array();
			foreach( $a_formData as $a_formInputName => $s_formInputValue ) {
				switch( $a_formInputName ) {
					case 'fullName':
						$this->a_displayNamesAndValues['Full Name'] = $s_formInputValue;
						break;
					case 'phone':
						$this->a_displayNamesAndValues['Phone Number'] = $s_formInputValue;
						break;
					default:
						$this->a_displayNamesAndValues[ ucfirst( $a_formInputName ) ] = $s_formInputValue;
						break;
				}
			}
		}
		
	}
	
	public function __destruct(){}
	
	/**
	 * This function formats the HTML and plain text email and then 
	 * sends the data to the sendEmail function
	 * 
	 * @return boolean
	 */
	public function formatEmail() {
		
		if( true === is_array( $this->a_displayNamesAndValues ) && false === empty( $this->a_displayNamesAndValues ) ) {
			
			$s_emailColumns = '';
			$s_plainTextEmailColumns = '';
			
			foreach( $this->a_displayNamesAndValues as $s_displayName => $s_inputValue ) {
				$s_emailColumns .= "<tr><td style=\"width:35%\"><strong>$s_displayName:</strong></td>
									<td> $s_inputValue </td></tr>";
				$s_plainTextEmailColumns .= " | $s_displayName: $s_inputValue \r\n";
			}
			
			$s_plainTextMessage = "Subject: $this->s_subject \r\n $s_plainTextEmailColumns";
			
			$s_message = 
<<<HTML
	<html>
		<body>
			<div align="center" style="padding:20px 0; color:#000;">
				<table rules="all" cellspacing="1" cellpadding="5" width="500" style="border:1px solid #666;font-size:14px;">
					<tr>
						<td colspan="2" style="font-size:16px; color:#fff; background-color:#7b7b7b;">$this->s_subject</td>
					</tr>
					$s_emailColumns
				</table>
			</div>
		</body>
	</html>
HTML;
			
			$a_formattedEmails = array(
				'message'			=> $s_message,
				'plainTextMessage'	=> $s_plainTextMessage
			);
			
			return $a_formattedEmails;
			
		}
		
		return false;
		
	}
	
	/**
	 * This function sends the form data in an HTML or plain text 
	 * email using PHPMailer
	 * 
	 * @param string $s_message
	 * @param string $s_plainTextMessage
	 * @return boolean
	 */
	public function sendEmail( $s_message, $s_plainTextMessage ){
		
		$o_mail = new PHPMailer(true);
		
		if( false === empty( $s_message ) || false == empty( $s_plainTextMessage ) ) {
			
			try {
				
	 		    //$o_mail->SMTPDebug = 3;		// Enable debug output
			    $o_mail->isSMTP();                                      
			    $o_mail->Host = $this->s_emailHost;
			    $o_mail->SMTPAuth = $this->s_smtpAuth;
			    $o_mail->Username = $this->s_emailUsername;
			    $o_mail->Password = $this->s_emailPass;
			    $o_mail->SMTPSecure = $this->s_smtpSecure;
			    $o_mail->Port = $this->i_emailPort;
			
			    if( false == empty( $this->s_fromName ) ) {
					$o_mail->setFrom( $this->s_fromEmail, $this->s_fromName );
			    }else{
					$o_mail->setFrom( $this->s_fromEmail );
			    }
			    $o_mail->addAddress( $this->s_toEmail );
			
			    $o_mail->isHTML(true);
			    $o_mail->Subject = $this->s_subject;
			    $o_mail->Body = $s_message;
			    $o_mail->AltBody = $s_plainTextMessage;
			    
	 		    $o_mail->send();
			    return true;
			    
			} catch ( Exception $e ) {
				
			    //echo 'Mailer Error: ' . $o_mail->ErrorInfo;
			    return false;
			    
			}
			
		}
		
		return false;
		
	}
	
}

?>