<?php
/**
 * This class does the necessary checks to determine if the data 
 * inputted into the given form is valid
 * 
 * @author Marissa Gasparro
 * @version 1.0
 */
class Validator {
	
	private $a_formData = array();
	private $a_inputFields = array();
	private $a_inputErrors = array();
	private $a_requiredInputFields = array();
	
	public function __construct(){}
	
	public function __destruct(){}
	
	/**
	 * This function validates any input from a given form
	 * 
	 * @param array $a_inputtedFormData
	 * @param array $a_inputtedFields
	 * @return boolean|array
	 */
	public function validateFields( $a_inputtedFormData, $a_inputtedFields ) {
		
		$this->a_inputErrors = array();
		
		if( true === is_array( $a_inputtedFormData ) && true == is_array( $a_inputtedFields ) ) {
			$this->a_formData = $a_inputtedFormData;
			$this->a_inputFields = $a_inputtedFields;
		}
		
		if( true === empty( $this->a_formData ) || true === empty( $this->a_inputFields ) ) {
			return false;
		}
		
		$this->a_requiredInputFields = array_keys( $this->a_inputFields, 'required' );
		
		foreach( $this->a_formData as $a_formInputName => $s_formInputValue ) {
			
			$a_formInputName = strip_tags( $a_formInputName );
			$s_formInputValue = strip_tags( $s_formInputValue );
			
			$s_validateFunction = 'validate' . ucfirst( $a_formInputName );
			
			if( true === method_exists( $this, $s_validateFunction ) ) {
				$s_formInputValue = trim( $s_formInputValue );
				
				if( true === empty( $s_formInputValue ) ) {
					if( true === in_array( $a_formInputName, $this->a_requiredInputFields ) ) {
						$this->a_inputErrors[$a_formInputName] = 'This field is required';
					}
					continue;
				}else{
					if( false == $this->$s_validateFunction( $s_formInputValue ) ) {
						$this->a_inputErrors[$a_formInputName] = 'This field is not valid';
					}
				}
				
			}else{
				$this->a_inputErrors[$a_formInputName] = 'This field is not valid';
			}
			
		}
			
		if( false == empty( $this->a_inputErrors ) ) {
			return array( $this->a_inputErrors );
		}
		
		return true;
		
	}
	
	
	/**
	 * This function validates the fullName input field
	 * 
	 * Note: This function checks for a space, assuming if there is no space a full name was
	 * not submitted. You could take out the check for a space if any name entered is more
	 * important than missing the lead.
	 * 
	 * @param string $s_fullName
	 * @return boolean
	 */
	public function validateFullName( $s_fullName ) {
		if( 1 > substr_count( $s_fullName, ' ' ) 
				|| 1 == preg_match( '/\d+/', $s_fullName ) 
				|| 1 == preg_match( '/[^a-zA-Z\s]/', $s_fullName ) ) {
			return false;
		}
		return true;
	}
	
	/**
	 * This function validates the email input field
	 * 
	 * @param string $s_email
	 * @return boolean
	 */
	public function validateEmail( $s_email ) {
		if (!filter_var($s_email, FILTER_VALIDATE_EMAIL)) {
			return false;
		}
		return true;
	}
	
	/**
	 * This function validates the phone input field
	 * 
	 * @param string $s_phone
	 * @return boolean
	 */
	public function validatePhone( $s_phone ) {
		$s_phoneNoWhitespaces = preg_replace('/\s+/', '', $s_phone);
		if( 1 != preg_match( '/((\+?[0-9]\s?)?(\.|\-)?\(?[0-9]{3}\)?(\s|\.|\-)?)?[0-9]{3}(\s|\.|\-)?[0-9]{4}/', $s_phoneNoWhitespaces ) ) {
			return false;
		}
		return true;
	}
	
	/**
	 * This function validates the message input field
	 * 
	 * @param string $s_message
	 * @return boolean
	 */
	public function validateMessage( $s_message ) {
		if( 1 >= strlen( $s_message ) ) {
			return false;
		}
		return true;
	}
	
}

?>