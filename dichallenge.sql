CREATE DATABASE  IF NOT EXISTS `dichallenge_db`;
CREATE USER 'dichallenge_user'@'localhost' IDENTIFIED BY '%NAP6s;MH4B\V3_!';
GRANT INSERT ON dichallenge.submissions TO 'dichallenge_user’@‘localhost' IDENTIFIED BY '%NAP6s;MH4B\V3_!';
USE `dichallenge_db`;
DROP TABLE IF EXISTS `submissions`;
CREATE TABLE `submissions` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `full_name` varchar(70) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `message` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `user_ip` varchar(45) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_agent` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `user_referer` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `email_sent` enum('false','true') CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT 'false',
  `to_email` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `ts_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
